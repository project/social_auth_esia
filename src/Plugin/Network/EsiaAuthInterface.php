<?php

namespace Drupal\social_auth_esia\Plugin\Network;

use Drupal\social_api\Plugin\NetworkInterface;

/**
 * Defines the ESIA Auth interface.
 */
interface EsiaAuthInterface extends NetworkInterface {}
